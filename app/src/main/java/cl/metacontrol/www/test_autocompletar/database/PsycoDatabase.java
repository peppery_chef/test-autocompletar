package cl.metacontrol.www.test_autocompletar.database;

import com.raizlabs.android.dbflow.annotation.Database;


@Database(name = PsycoDatabase.NAME, version = PsycoDatabase.VERSION)
public class PsycoDatabase {
    public static final String NAME = "PsycoDB";
    public static final int VERSION = 1;

}


