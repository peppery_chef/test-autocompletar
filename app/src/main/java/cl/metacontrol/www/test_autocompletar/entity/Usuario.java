package cl.metacontrol.www.test_autocompletar.entity;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import cl.metacontrol.www.test_autocompletar.database.PsycoDatabase;


@Table(database = PsycoDatabase.class)
public class Usuario extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    private int id;

    @Column
    private String nombre;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public static Usuario getUserByNombre(String nombre){

        Usuario tmp;

        tmp = SQLite.select()
                .from(Usuario.class)
                .where(Usuario_Table.nombre.eq(nombre))
                .querySingle();

        if (tmp != null)
            return tmp;
        else
            return new Usuario();
    }

    public static Usuario getUserById(int id){

        Usuario tmp;

        tmp = SQLite.select()
                .from(Usuario.class)
                .where(Usuario_Table.id.eq(id))
                .querySingle();

        if (tmp != null)
            return tmp;
        else
            return new Usuario();
    }

    public static List<Usuario> getListUsuarios(){

        List<Usuario> tmpList;

        tmpList = SQLite.select()
                .from(Usuario.class)
                .queryList();

        return tmpList;
    }

    public static Usuario getLastUser(){

        Usuario tmp;

        List<Usuario> tmpList;

        tmpList = SQLite.select()
                .from(Usuario.class)
                .queryList();

        if (!tmpList.isEmpty())
            tmp = tmpList.get(tmpList.size()-1);
        else
            tmp = new Usuario();

        return tmp;
    }

    public static String[] stringUsuarios(){

        List<Usuario> listTemp = getListUsuarios();

        String[] stockArr;

        if (!listTemp.isEmpty()){

            stockArr= new String[listTemp.size()];

            for (int i = 0; i < listTemp.size(); i++) {
                stockArr[i] = listTemp.get(i).nombre;
            }


        }else
            stockArr = new String[0];

        return stockArr;

    }


}
