package cl.metacontrol.www.test_autocompletar;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import cl.metacontrol.www.test_autocompletar.entity.Usuario;

public class MainActivity extends AppCompatActivity {

    private AnimationDrawable animationDrawable;

    private AutoCompleteTextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btnSignIn;
        ImageView animLayout;
        TextView txtVersion;

        txtVersion = findViewById(R.id.txt_version);

        FlowManager.init(new FlowConfig.Builder(this).build());

        btnSignIn = findViewById(R.id.btn_SignIn);
        animLayout = findViewById(R.id.anim_layout);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // initializing animation drawable by getting background from constraint layout
        animationDrawable = (AnimationDrawable) animLayout.getDrawable();

        // setting enter fade animation duration to 5 seconds
        animationDrawable.setEnterFadeDuration(500);

        // setting exit fade animation duration to 5 seconds
        animationDrawable.setExitFadeDuration(500);

        try {
            PackageInfo info = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionCode = info.versionCode;
            String versionName = info.versionName;
            String strCodigo;
            strCodigo = String.valueOf(getText(R.string.version_code));
            strCodigo += String.valueOf(versionCode);
            strCodigo += "\n";
            strCodigo += String.valueOf(getText(R.string.version_name));
            strCodigo += versionName;
            txtVersion.setText(strCodigo);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
                |WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        btnSignIn.setOnClickListener(clickLogin);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, Usuario.stringUsuarios());
        textView = findViewById(R.id.etCod);
        textView.setAdapter(adapter);
        textView.setOnEditorActionListener(new DoneOnEditorActionListener());

        ActionBar bar = MainActivity.this.getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.bg_screen1)));

        ultimoUsuario();

    }

    private void ultimoUsuario(){

        Usuario tmp = Usuario.getLastUser();

        if (tmp.getId()!= 0){
            textView.setText(tmp.getNombre());
        }

    }

    private void checkNombre (){

        String nombre = textView.getText().toString();

        Usuario usrTmp = Usuario.getUserByNombre(nombre);

        if (usrTmp.getId() == 0){
            usrTmp.setNombre(nombre);
            usrTmp.save();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            // start the animation
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            // stop the animation
            animationDrawable.stop();
        }
    }

    @Override
    public void onBackPressed(){

        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);

    }

    View.OnClickListener clickLogin = new View.OnClickListener() {


        private boolean checkText(){

            String nombre = textView.getText().toString();
            nombre = nombre.replace(" ","");
            if (nombre.equals("")){
                Toast.makeText(MainActivity.this, "Debes ingresar un nombre", Toast.LENGTH_LONG).show();
                return false;
            }else
                return true;


        }

        @Override
        public void onClick(View v) {

            if (checkText()) {

                checkNombre();

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        }
    };
}
